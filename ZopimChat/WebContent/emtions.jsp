<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function openImotion(){
        document.getElementById('dpop').style.display = "block";

    }

function appendText(elementId,val){
        var chatbox = document.f.chatbox;
        chatbox.value += val;
        document.getElementById('dpop').style.display = "none";
    }
</script>
</head>
<body>
<tr>
        <td colspan="3" width="100%">
            <div style="margin-left: 0px; margin-top: 0px; margin-bottom: 20px;">
                <div>
                    <img class="trigger" src="images/emoticons/happy.gif" class="pointer" onclick="javascript:openImotion();" 
                        id="download" alt="emotion"/>
               </div>
                <table id="dpop" class="popup" style="display: none;">
                    <tbody>
                        <tr>
                            <td id="topleft" class="corner"></td>
                            <td class="top"></td>
                            <td id="topright" class="corner"></td>
                        </tr>
                        <tr>
                            <td class="left"></td>
                            <td><table class="popup-contents">
                                    <tbody>
                                        <tr>
                                            <td><img src="images/emoticons/angry.gif" title="angry"
                                                onclick="appendText('chatbox','>:o ');" border="0" /> <img
                                                src="images/emoticons/blush.gif" title="blush"
                                                onclick="appendText('chatbox',':-[ ');" border="0" /> <img
                                                src="images/emoticons/confused.gif" title="confused"
                                                onclick="appendText('chatbox','?:| ');" border="0" /> <img
                                                src="images/emoticons/cool.gif" title="cool"
                                                onclick="appendText('chatbox','B-) ');" border="0" /> <img
                                                src="images/emoticons/cry.gif" title="cry"
                                                onclick="appendText('chatbox',':_| ');" border="0" /> <img
                                                src="images/emoticons/devil.gif" title="devil"
                                                onclick="appendText('chatbox',']:) ');" border="0" /> <img
                                                src="images/emoticons/grin.gif" title="grin"
                                                onclick="appendText('chatbox',':-D ');" 
                                                border="0" />
                                            <td>
                                        </tr>
                                        <tr>
                                            <td><img src="images/emoticons/happy.gif" title="happy"
                                                onclick="appendText('chatbox',':-) ');" 
                                                border="0" /> <img src="images/emoticons/laugh.gif"
                                                onclick="appendText('chatbox',':^0 ');" border="0" /> <img
                                                src="images/emoticons/love.gif" title="love"
                                                onclick="appendText('chatbox',':x ');" border="0" /> <img
                                                src="images/emoticons/mischief.gif" title="mischief"
                                                onclick="appendText('chatbox',';\\ ');" border="0" /> <img
                                                src="images/emoticons/plain.gif"  title="plain"
                                                onclick="appendText('chatbox',':| ');" border="0" /> <img
                                                src="images/emoticons/happy.gif" title="smile"
                                                onclick="appendText('chatbox',':-) ');" border="0" /> <img
                                                src="images/emoticons/sad.gif" title="sad"
                                                onclick="appendText('chatbox',':-( ');" border="0" /></td>
                                        </tr>
                                        <tr>
                                            <td><img src="images/emoticons/shocked.gif" title="shocked"
                                                onclick="appendText('chatbox',':0 ');" border="0" /> <img
                                                src="images/emoticons/silly.gif" title="silly"
                                                onclick="appendText('chatbox',':-p ');" border="0" /> <img
                                                src="images/emoticons/wink.gif" title="wink"
                                                onclick="appendText('chatbox',';-) ');" border="0" /> <img
                                                src="images/emoticons/info.png" title="info"
                                                onclick="appendText('chatbox','(i) ');" border="0" /> <img
                                                src="images/emoticons/plus.png" title="plus"
                                                onclick="appendText('chatbox','(+) ');" border="0" /> <img
                                                src="images/emoticons/minus.png" title="minus"
                                                onclick="appendText('chatbox','(-) ');" border="0" /> <img
                                                src="images/emoticons/heart.png" title="heart"
                                                onclick="appendText('chatbox','(heart) ');" border="0" /></td>
                                        </tr>
                                    </tbody>
                                </table></td>
                            <td class="right"></td>
                        </tr>
                        <tr>
                            <td class="corner" id="bottomleft"></td>
                            <td class="bottom"><img width="30" height="29"
                                alt="popup tail" src="images/bubble/bubble-tail2.png" />
                            </td>
                            <td id="bottomright" class="corner"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--<div style="margin-left:100px;margin-top:-20px;">
                <img src="images/file-transfer.png" onclick="javascript:sendFile();" width="20" height="20" alt="File Transfer"/>
            </div>
            -->
        </td>
    </tr>
    <br>
    <tr><td colspan="3"></td></tr>
    <tr><td colspan="3"></td></tr>
</body>
</html>